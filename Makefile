ifndef ENVIRONMENT
ENVIRONMENT=local
endif

help:
	@echo "Environment: "$(ENVIRONMENT)
	@echo ""
	@echo "Docker"
	@echo "make build"
	@echo "make run"
	@echo "make kill"
	@echo ""
	@echo "Kubernetes"
	@echo "k8s-get"
	@echo "k8s-init"
	@echo "k8s-apply"
	@echo "k8s-delete"
init:
	git clone $(REPO) $(LOCAL)/src/
	$(foreach var,$(shell ls Environment.*|awk -F "." '{print $$2}'), mkdir -p work/$(var);)
	$(foreach var,$(shell ls Environment.*|grep -v local|awk -F "." '{print $$2}'), mkdir -p work/$(var)/k8s;)

clean:
	rm -rf work/*

build:
	cp docker/* $(LOCAL)
	cd $(LOCAL) && docker build --rm -t $(IMAGE) .

tag:
	cd $(LOCAL) && docker tag $(IMAGE) $(REG)/$(IMAGE)

push:
	cd $(LOCAL) && docker push $(REG)/$(IMAGE)

run:
	cp $(ENV) $(LOCAL)
	cd $(LOCAL) && docker run -d --name $(TAG) --env-file $(ENV) $(DOCKER_PORTS) $(IMAGE)

dev:
	cp Environment.local $(LOCAL)
	cd $(LOCAL) && docker run -it --mount type=bind,source=$(LOCAL)/$(BASE_REPO),target=/var/www/html --name $(TAG) --env-file $(ENV) $(DOCKER_PORTS) $(IMAGE) bash
	docker rm $(TAG)

kill:
	docker kill $(TAG)
	docker rm $(TAG)

k8s-init:
	cd work/$(ENVIRONMENT)/k8s && $(KBCTL) create secret generic $(K8STAG)-sec $(foreach var,$(shell cat $(ENV)|xargs -d "\n" echo), --from-literal=$(var)) --dry-run -o yaml > $(K8STAG).sec.yaml
	$(foreach var,$(shell ls k8s/*.yaml), envsubst < ${var} > work/$(ENVIRONMENT)/${var};)

k8s-apply:
	cd work/$(ENVIRONMENT)/k8s && $(KBCTL) apply -f .

k8s-shell:
	cd work/$(ENVIRONMENT)/k8s && $(KBCTL) exec -it $(K8STAG)-0 ash

k8s-delete:
	cd work/$(ENVIRONMENT)/k8s && $(KBCTL) delete -f .

k8s-get:
	cd work/$(ENVIRONMENT)/k8s && $(KBCTL) get -f .


LOCAL= $(PWD)/work/local
REPO= https://bitbucket.org/summit_team/flask-restless-scaffold.git
BASE_REPO= src
REG=
TAG= example
IMAGE= $(TAG):$(ENVIRONMENT)
ENV= ./Environment.$(ENVIRONMENT)
NAMESPACE= $(ENVIRONMENT)
KBCTL= kubectl --namespace=$(NAMESPACE)
K8STAG=$(TAG)
SHELL := /bin/bash
# export all following MAkefile vars to the environment
export
include $(ENV)
